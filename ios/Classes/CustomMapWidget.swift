//
//  CustomMapWidget.swift
//  yandex_mapkit
//
//  Created by Hightech on 09.03.2023.
//

import CoreGraphics
import UIKit

class CustomMapWidget: UICollectionViewCell {
    let textView: UITextView = {
        let tv = UITextView()
        tv.text = "Some text"
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .clear
        return tv
    }()

    let bubbleView: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        tempView.backgroundColor = UIColor.white
        tempView.layer.cornerRadius = 6
        return tempView
    }()

    let buttonBackground: UIView = {
            let tempView = UIView()
            tempView.translatesAutoresizingMaskIntoConstraints = false
            tempView.backgroundColor = UIColor(red: 0.18, green: 0.48, blue: 0.85, alpha: 1.0)
            tempView.layer.cornerRadius = 6
            return tempView
    }()

    let buttonTextView: UITextView = {
        let tv = UITextView()
        tv.text = "Перейти"
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .clear
        tv.textColor = UIColor.white
        return tv
    }()

    let bottomTongue: UIView = {
        let tempView = UIView()
        tempView.translatesAutoresizingMaskIntoConstraints = false
        tempView.backgroundColor = UIColor.white

        let degrees = 45.0

        tempView.transform = CGAffineTransformMakeRotation(degrees * M_PI/180)
        return tempView
    }()

    let buttonWidth: CGFloat = 80
    let buttonHeight: CGFloat = 25

    init(frame: CGRect, text: String, frameWidth: CGFloat, frameHeight: CGFloat) {
        super.init(frame: frame)

        isOpaque = false
        backgroundColor = UIColor.clear.withAlphaComponent(0.0)

        addSubview(bubbleView)
        addSubview(bottomTongue)
        addSubview(textView)
        addSubview(buttonBackground)
        addSubview(buttonTextView)

        bubbleView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bubbleView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        bubbleView.widthAnchor.constraint(equalToConstant: frameWidth).isActive = true
        bubbleView.heightAnchor.constraint(equalTo: heightAnchor, constant: -20).isActive = true

        textView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        textView.widthAnchor.constraint(equalToConstant: frameWidth).isActive = true
        textView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        textView.text = text

        buttonBackground.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        buttonBackground.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -25).isActive = true
        buttonBackground.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        buttonBackground.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true

        buttonTextView.leftAnchor.constraint(equalTo: buttonBackground.leftAnchor).isActive = true
        buttonTextView.bottomAnchor.constraint(equalTo: buttonBackground.bottomAnchor, constant: -5).isActive = true
        buttonTextView.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        buttonTextView.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true

        bottomTongue.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        bottomTongue.bottomAnchor.constraint(equalTo: bubbleView.bottomAnchor, constant: 10).isActive = true
        bottomTongue.widthAnchor.constraint(equalToConstant: 20).isActive = true
        bottomTongue.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented yet")
    }
}
