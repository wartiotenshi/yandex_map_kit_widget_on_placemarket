import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:yandex_mapkit_example/examples/widgets/map_page.dart';

class SomeTestPage extends MapPage {
  const SomeTestPage() : super("Test page created by Koyash");

  @override
  Widget build(BuildContext context) {
    return _SomeTestPage();
  }
}

class _SomeTestPage extends StatefulWidget {
  const _SomeTestPage({Key? key}) : super(key: key);

  @override
  State<_SomeTestPage> createState() => _SomeTestpageState();
}

class MyPoint {
  double latitude;
  double longitude;
  bool isVisible;

  MyPoint({
    required this.latitude,
    required this.longitude,
    required this.isVisible,
  });
}

class _SomeTestpageState extends State<_SomeTestPage> {
  final List<MapObject> mapObjects = [];
  final MapObjectId mapObjectId =
      MapObjectId('clusterized_placemark_collection');
  final List<MyPoint> points = [
    MyPoint(latitude: 55.756, longitude: 37.618, isVisible: false),
    MyPoint(latitude: 64.956, longitude: 28.313, isVisible: false),
  ];

  List<PlacemarkMapObject> placemarks = [];

  String selectedText = '';

  @override
  void initState() {
    updatePlacemarks();

    super.initState();
  }

  void updatePlacemarks() {
    placemarks.clear();

    for (var i = 0; i < points.length; i++) {
      placemarks.add(PlacemarkMapObject(
        zIndex: 1.0,
        mapId: MapObjectId('placemark_$i'),
        point:
            Point(latitude: points[i].latitude, longitude: points[i].longitude),
        icon: PlacemarkIcon.single(PlacemarkIconStyle(
            image: BitmapDescriptor.fromAssetImage('lib/assets/place.png'),
            scale: 1)),
        onTap: (mapObject, point) {
          setState(() {
            points[i].isVisible = !points[i].isVisible;
            final newWidget = PlacemarkMapObject(
              opacity: 1,
              zIndex: 2.0,
              consumeTapEvents: true,
              mapId: MapObjectId(
                'obj_\"$i\"',
              ),
              isVisible: points[i].isVisible,
              point: Point(
                latitude: points[i].latitude,
                longitude: points[i].longitude,
              ),
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                  image:
                      BitmapDescriptor.fromAssetImage('lib/assets/place.png'),
                  scale: 1)),
              onTap: (mapObject, point) {
                setState(() {
                  selectedText = '${mapObject.mapId}';
                });
              },
            );
            setState(() {
              mapObjects.add(newWidget);
            });
          });
        },
      ));
    }

    updateMapClusterObject();
  }

  void updateMapClusterObject() {
    final mapObject = ClusterizedPlacemarkCollection(
      mapId: mapObjectId,
      radius: 30,
      minZoom: 15,
      onClusterAdded:
          (ClusterizedPlacemarkCollection self, Cluster cluster) async {
        return cluster.copyWith(
          appearance: cluster.appearance.copyWith(
            icon: PlacemarkIcon.single(
              PlacemarkIconStyle(
                  image:
                      BitmapDescriptor.fromAssetImage('lib/assets/cluster.png'),
                  scale: 1),
            ),
          ),
        );
      },
      placemarks: placemarks,
      onTap: (ClusterizedPlacemarkCollection self, Point point) {},
    );

    setState(() {
      mapObjects.add(mapObject);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          child: Container(
            child: YandexMap(
              mapObjects: mapObjects,
            ),
          ),
        ),
        SizedBox(height: 20),
        Expanded(
          child: Text(selectedText),
        ),
      ],
    );
  }
}
